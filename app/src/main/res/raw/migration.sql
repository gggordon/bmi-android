create table bmi_person(
    id INTEGER PRIMARY KEY AUTOINCREMENT ,
    username INTEGER,
    password varchar(80),
    healthCareNo varchar(40),
    dateOfBirth date,
    agreeToDisclaimer bit default 0
);

create table bmi_entry (
   id INTEGER PRIMARY KEY AUTOINCREMENT ,
   person_id INTEGER,
   height decimal(11,2),
   weight decimal(11,2),
   dateRecorded date,
   FOREIGN KEY(person_id) references bmi_person(id)
);

