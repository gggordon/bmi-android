package com.igonics.ontrackbmi.adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.igonics.ontrackbmi.R;
import com.igonics.ontrackbmi.fragments.BMIListItemFragment;
import com.igonics.ontrackbmi.models.BMIEntry;

import java.util.List;

/**
 * Created by ggordon on 2016-03-31.
 */
public class BMIEntryAdapter extends ArrayAdapter<BMIEntry> {

    public BMIEntryAdapter(Context context, List<BMIEntry> entries){
        super(context,0,entries);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        BMIEntry entry = getItem(position);
        try {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            Bundle details = new Bundle();
            details.putSerializable(BMIListItemFragment.ARG_BMIEntry, entry);
            BMIListItemFragment fragment = BMIListItemFragment.newInstance(entry);
            View view = fragment.onCreateView(inflater,parent,details);

            return view;

        }catch(Exception e){
            e.printStackTrace();
            return  convertView;
        }


        //return null;

    }
}
