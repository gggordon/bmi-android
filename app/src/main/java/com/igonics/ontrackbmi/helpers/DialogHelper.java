package com.igonics.ontrackbmi.helpers;

import android.app.FragmentManager;
import android.os.Bundle;

import com.igonics.ontrackbmi.fragments.TextPromptDialog;

/**
 * Created by ggordon on 2016-03-31.
 */
public class DialogHelper {

    public static TextPromptDialog promptText(FragmentManager fm,String title){
        return promptText(fm,title,"","");
    }

    public static TextPromptDialog promptText(FragmentManager fm,String title, String prompt, String hint){
        return promptText(fm,title,"","",null,null);

    }

    public static TextPromptDialog promptText(FragmentManager fm,String title, String prompt,
                                              String hint,
                                              TextPromptDialog.TextPromptDialogListener onSaveListener,
                                              TextPromptDialog.TextPromptDialogListener onCancelListener){
        Bundle config = new Bundle();
        config.putString(TextPromptDialog.ARG_TITLE,title);
        config.putString(TextPromptDialog.ARG_PROMPT, prompt);
        config.putString(TextPromptDialog.ARG_HINT, hint);
        config.putSerializable(TextPromptDialog.ARG_ON_CANCEL_LISTENER, onCancelListener);
        config.putSerializable(TextPromptDialog.ARG_ON_SAVE_LISTENER, onSaveListener);

        TextPromptDialog _prompt = new TextPromptDialog();
        _prompt.setArguments(config);
        _prompt.show(fm, null);
        return _prompt;

    }
}
