package com.igonics.ontrackbmi.helpers;

import android.app.Activity;
import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

/**
 * Created by ggordon on 2016-03-31.
 */
public class OwnerHelper {

    private static String displayName;
    private static Uri photoUri;
    private static long photoId;
    private static boolean isValidPhotoId;

    public static Uri getContactPhoto(Activity activity){
        if(photoUri == null || !isValidPhotoId)
            updateOwnerDetails(activity);
        return photoUri;
    }

    public static boolean isIsValidPhotoId(){
        
        return isValidPhotoId;
    }

    public  static String getDisplayName(Activity activity){
        if(displayName == null)
            updateOwnerDetails(activity);
        return displayName;
    }

    private static void updateOwnerDetails(Activity activity){
        String[] columnNames = new String[] {ContactsContract.Profile.DISPLAY_NAME, ContactsContract.Profile.PHOTO_ID};

        Cursor c = activity.getContentResolver().query(ContactsContract.Profile.CONTENT_URI, columnNames, null, null, null);
        int count = c.getCount();
        boolean b = c.moveToFirst();
        int position = c.getPosition();
        if (count == 1 && position == 0) {
            for (int j = 0; j < columnNames.length; j++) {
                displayName = c.getString(0);
                photoId = c.getLong(1);
            }
        }
        c.close();
        isValidPhotoId = ContactsContract.isProfileId(photoId);

        photoUri = ContentUris.withAppendedId(ContactsContract.Data.CONTENT_URI,
                photoId);
    }


}
