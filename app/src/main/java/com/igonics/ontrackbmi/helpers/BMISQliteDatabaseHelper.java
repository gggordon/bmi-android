package com.igonics.ontrackbmi.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.content.ContentValues;

import com.igonics.ontrackbmi.R;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.Date;

/**
 * Created by ggordon on 2016-03-29.
 */
public class BMISQliteDatabaseHelper extends SQLiteOpenHelper{

    private static final String DB_NAME = "ontrackbmi";
    private static final int DB_VERSION = 3;

    protected Context context;


    public BMISQliteDatabaseHelper(Context context){
        super(context,DB_NAME,null, DB_VERSION);
        this.context = context;
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        runMigrations(db);

    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            //run cleanup
            runScripts(db,getScriptsFromResourceWithId(R.raw.destroy));
            runMigrations(db);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void runMigrations(SQLiteDatabase db){
        String[] migrationScripts = getScriptsFromResourceWithId(R.raw.migration);
        try {
            runScripts(db,migrationScripts);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void runScripts(SQLiteDatabase db, String[] scripts) throws SQLException {
        if(scripts != null)
            for(String query : scripts)
                db.execSQL(query);

    }

    protected String[] getScriptsFromResourceWithId(int resourceId){

        String[] migrationScripts = null;

        try {

            InputStream inputStream = context.getResources().openRawResource(resourceId);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader br = new BufferedReader(inputStreamReader);
            String fileContents = "";
            String line;
            while( (line = br.readLine()) != null ){
                fileContents+=line+" ";
            }
            br.close();

            migrationScripts = fileContents.replaceAll("\n"," ").trim().split(";");

        } catch (IOException e) {
            e.printStackTrace();
        }
        return  migrationScripts;
    }


}
