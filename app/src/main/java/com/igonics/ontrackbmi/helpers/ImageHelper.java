package com.igonics.ontrackbmi.helpers;

import android.app.Activity;
import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by ggordon on 2016-03-31.
 */
public class ImageHelper {
    public static Bitmap getImageBitmap(String url) {
        Bitmap bm = null;
        try {
            URL aURL = new URL(url);
            URLConnection conn = aURL.openConnection();
            conn.connect();
            InputStream is = conn.getInputStream();
            BufferedInputStream bis = new BufferedInputStream(is);
            bm = BitmapFactory.decodeStream(bis);
            bis.close();
            is.close();
        } catch (IOException e) {
            Log.e("ImageHelper", "Error getting bitmap", e);
        }
        return bm;
    }

    public static Bitmap resolveContentUri(Activity activity, Uri uri){
        Bitmap image = null;
        try {
            //image = MediaStore.Images.Media.getBitmap(activity.getContentResolver(), uri);
            ParcelFileDescriptor p=activity.getContentResolver().openFileDescriptor(uri, "r");
            image = BitmapFactory.decodeFileDescriptor(p.getFileDescriptor());

        } catch (FileNotFoundException  e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }
        return  image;

    }
}
