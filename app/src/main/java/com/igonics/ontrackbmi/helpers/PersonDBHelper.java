package com.igonics.ontrackbmi.helpers;

import android.content.Context;
import android.database.Cursor;

import com.igonics.ontrackbmi.models.Person;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ggordon on 2016-03-29.
 */
public class PersonDBHelper extends BMISQliteDatabaseHelper {

    private static final String updatePersonSQL;
    private static final String addPersonSQL;
    private static final String getPersonsSQL;
    private static final String getFirstPersonSQL;

    private static final String PERSON_TABLE;
    private static final String ALL_FIELD_NAMES;


    public PersonDBHelper(Context context) {
        this(context, false);
    }

    public PersonDBHelper(Context context, boolean ensureDefaultAccount) {
        super(context);
        if(ensureDefaultAccount){
           if(getPersonCount()  < 1)
               addPerson(new Person());
        }
    }

    public Person getFirstPerson(){
        Person person = null;

        try{
            Cursor cursor = this.getReadableDatabase().rawQuery(getFirstPersonSQL,null);
            while(cursor.moveToNext()) {

                person = new Person(
                        cursor.getInt(0),
                        cursor.getString(1),
                        cursor.getString(2),
                        Date.valueOf(cursor.getString(4)),
                        cursor.getInt(5) == 1,
                        cursor.getString(3),
                        null
                );
            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }

        return  person;
    }

    public List<Person> getAll(){
        List<Person> persons = new ArrayList<Person>();
        try{
            Cursor cursor = this.getReadableDatabase().rawQuery(getPersonsSQL, null);
            cursor.getCount();
            cursor.close();
        }catch (Exception e){

        }
        return persons;
    }

    public int getPersonCount(){
        int count = -1;
        try{
            Cursor cursor = this.getReadableDatabase().rawQuery(getPersonsSQL, null);
            count=cursor.getCount();
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
            count = -1;
        }
        return count;
    }

    public boolean addPerson(Person person){

        boolean success = false;
        try{
            if(person.dateOfBirth == null)
                person.dateOfBirth = new java.util.Date();
            this.getWritableDatabase()
                    .execSQL(addPersonSQL, new Object[]{
                            person.username,
                            person.password,
                            person.healthCareNo,
                            new Date(person.dateOfBirth.getTime()),
                            person.agreeToDisclaimer ? 1 : 0
                    });
            success = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return success;
    }

    public boolean updatePerson(Person person){

        boolean success = false;
        try{
            if(person.dateOfBirth == null)
                person.dateOfBirth = new java.util.Date();
            Date dateOfBirth = new Date(person.dateOfBirth.getTime());
            this.getWritableDatabase()
                    .execSQL(updatePersonSQL, new Object[]{
                            person.username,
                            person.password,
                            person.healthCareNo,
                            dateOfBirth,
                            person.agreeToDisclaimer ? 1 : 0,
                            person.id
                    });
            success = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return success;
    }

    static {
        PERSON_TABLE="bmi_person";
        ALL_FIELD_NAMES = "id, username, password, healthCareNo, dateOfBirth, agreeToDisclaimer";

        //Queries
        updatePersonSQL = "update "+PERSON_TABLE +" set username = ? , password = ?, healthCareNo = ?, dateOfBirth = ? , agreeToDisclaimer = ? where id = ?";
        getPersonsSQL="select "+ALL_FIELD_NAMES+" from "+PERSON_TABLE;
        getFirstPersonSQL = getPersonsSQL+" limit 0,1";
        addPersonSQL="insert into "+PERSON_TABLE+" (  username, password, healthCareNo, dateOfBirth, agreeToDisclaimer) values (?, ? , ?, ? , ?)";

    }
    //https://github.com/jehy/Android-GetOwnerInfo/blob/master/Android-get-owner-contact-info.java

    //http://www.vogella.com/tutorials/AndroidListView/article.html


}
