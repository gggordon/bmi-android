package com.igonics.ontrackbmi.helpers;

import android.content.Context;
import android.database.Cursor;

import com.igonics.ontrackbmi.models.BMIEntry;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ggordon on 2016-03-31.
 */
public class BMIDBHelper  extends BMISQliteDatabaseHelper {

    private static final String getPersonEntriesSQL;
    private static final String getAllEntriesSQL;
    private static final String addEntrySQL;

    private static final String BMI_TABLE;
    private static final String ALL_FIELD_NAMES;

    public BMIDBHelper(Context context) {
        super(context);
    }

    public List<BMIEntry> getAllEntries(){
        List<BMIEntry> entries = new ArrayList<BMIEntry>();
        try{
            Cursor cursor = this.getReadableDatabase().rawQuery(getAllEntriesSQL,null);
            while(cursor.moveToNext()) {

                entries.add(new BMIEntry(
                        cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getFloat(2),
                        cursor.getFloat(3),
                        Date.valueOf(cursor.getString(4))

                ));

            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return entries;
    }

    public List<BMIEntry> getAllPersonEntries(Integer personId){
        List<BMIEntry> entries = new ArrayList<BMIEntry>();
        try{
            Cursor cursor = this.getReadableDatabase().rawQuery(getPersonEntriesSQL,new String[]{personId.toString()});
            while(cursor.moveToNext()) {
                String c = cursor.getString(4);
                entries.add(new BMIEntry(
                        cursor.getInt(0),
                        cursor.getInt(1),
                        cursor.getFloat(2),
                        cursor.getFloat(3),
                        Date.valueOf(cursor.getString(4))

                ));

            }
            cursor.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return entries;
    }

    public boolean addEntry(BMIEntry entry){

        boolean success = false;
        try{
            if(entry.dateRecorded == null)
                entry.dateRecorded = new java.util.Date();
            this.getWritableDatabase()
                    .execSQL(addEntrySQL, new Object[]{
                            entry.personId,
                            entry.height,
                            entry.weight,
                            new java.sql.Date(entry.dateRecorded.getTime())
                    });
            success = true;
        }catch(Exception e){
            e.printStackTrace();
        }
        return success;
    }

    static {
        BMI_TABLE="bmi_entry";
        ALL_FIELD_NAMES=" id, person_id, weight,  height, dateRecorded ";
        getPersonEntriesSQL = "select "+ALL_FIELD_NAMES+" from "+BMI_TABLE+" where person_id = ? order by dateRecorded desc";
        getAllEntriesSQL = "select "+ALL_FIELD_NAMES+" from "+BMI_TABLE+" order by dateRecorded desc";
        addEntrySQL=" insert into "+BMI_TABLE+" ( person_id, height, weight, dateRecorded ) values ( ? ,? ,? ,?)";
    }

}
