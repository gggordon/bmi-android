package com.igonics.ontrackbmi.models;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by ggordon on 2016-03-28.
 */
public class BMIEntry implements Serializable{
    public int id;
    public int personId;
    public float weight;
    public float height;
    public Date dateRecorded;

    public BMIEntry(){
        this(0,0);
    }

    public  BMIEntry(float weight, float height){
        this(weight,height, new Date());
    }


    public  BMIEntry(float weight, float height, Date dateRecorded){
        this(0,0,weight,height,dateRecorded);
    }

    public  BMIEntry(int id, int personId, float weight, float height, Date dateRecorded){
        super();
        this.id = id;
        this.personId = personId;
        this.weight = weight;
        this.height = height;
        this.dateRecorded = dateRecorded;
    }

    public float getBMI(){
        return height == 0 ? 0 : (  weight / (height * height)  );
    }

    public String getDateRecorded(){
        return String.format("%d/%d/%d",dateRecorded.getDay(), dateRecorded.getMonth(), dateRecorded.getYear()+1900);
    }

    public String getStatus(){
        float bmi = getBMI();
        if (bmi <= 18.5)  return "Under weight" ;
        if (bmi > 18.5 && bmi <= 24.9 ) return "Normal" ;
        if (bmi > 24.9 && bmi <= 29.9) return "Over weight" ;
        return "Grossly over weight";

    }

    @Override
    public String toString(){
        return getStatus() +" : "+getBMI();
    }


}
