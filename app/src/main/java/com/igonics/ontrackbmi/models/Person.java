package com.igonics.ontrackbmi.models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by ggordon on 2016-03-28.
 */
public class Person {
    public int id;
    public String username;
    public String password;
    public Date dateOfBirth;
    public String healthCareNo;
    public boolean agreeToDisclaimer;
    private List<BMIEntry> records;

    public Person(){
        this("");
    }

    public Person(String username){
        this(username,"",new Date(),false,"", new ArrayList<BMIEntry>());
    }

    public Person(String username, String password, Date dateOfBirth,
                  boolean agreeToDisclaimer, String healthCareNo, List<BMIEntry> records){
        this(0,username,"",new Date(),false,"", new ArrayList<BMIEntry>());
    }

    public Person(int id, String username, String password, Date dateOfBirth,
                  boolean agreeToDisclaimer, String healthCareNo, List<BMIEntry> records){
        super();
        this.id = id;
        this.username = username;
        this.password = password;
        this.dateOfBirth = dateOfBirth;
        this.agreeToDisclaimer = agreeToDisclaimer;
        this.healthCareNo = healthCareNo;
        if(records != null)
            this.records = records;
        else
            this.records = new ArrayList<BMIEntry>();
    }

    public void addRecord(BMIEntry record){
        records.add(0,record);
    }

    public void addRecord(BMIEntry record, int index){
        records.add(index, record);
    }

    public boolean removeRecord(BMIEntry record){
        return records.remove(record);
    }

    public boolean removeRecord(int index){
        return records.remove(index) != null;
    }

    public void addRecords(List<BMIEntry> entries){
        records.addAll(0,entries);
    }

    public List<BMIEntry> getRecords(){
        return records;
    }

    public BMIEntry getMostRecentBMI(){
        BMIEntry mostRecent = null;

        List<BMIEntry> records = getRecords();
        if(records.size()==0)
            return mostRecent;
        Collections.sort(records, new
                Comparator<BMIEntry>() {
                    @Override
                    public int compare(BMIEntry lhs, BMIEntry rhs) {
                        if (lhs == null || rhs == null)
                            return 1;
                        if (lhs.dateRecorded == null || rhs.dateRecorded == null)
                            return 0;
                        if (lhs.dateRecorded.before(rhs.dateRecorded))
                            return 0;
                        return 0;
                    }
                });

        return records.get(0);
    }






}
