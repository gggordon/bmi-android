package com.igonics.ontrackbmi;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.github.fabtransitionactivity.SheetLayout;
import com.igonics.ontrackbmi.adapters.BMIEntryAdapter;
import com.igonics.ontrackbmi.fragments.BMIListItemFragment;
import com.igonics.ontrackbmi.models.BMIEntry;
import com.igonics.ontrackbmi.models.Person;
import com.igonics.ontrackbmi.repos.BMIDataManager;
import com.shamanland.fab.FloatingActionButton;

public class BMIListActivity extends AppBMIActivity implements BMIListItemFragment.OnFragmentInteractionListener{
    Person person;

    ListView listView;
    private enum FabClickedEnum {
        AddBMIEntry,
        Profile
    }

    private FloatingActionButton btnAddBMIEntry;
    private FloatingActionButton btnUpdateProfile;
    private FabClickedEnum fabClicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmilist);
        person = BMIDataManager.getPerson(this);

        configureView();
        configureEvents();
    }

    public void configureView(){
        listView = (ListView) findViewById(R.id.listView);
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                return false;
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        BMIEntryAdapter bmiEntryArrayAdapter = new BMIEntryAdapter(
                this,
                person.getRecords()
                );
        listView.setAdapter(bmiEntryArrayAdapter);

    }

    public void configureEvents(){
        btnAddBMIEntry = (FloatingActionButton) findViewById(R.id.btnAddBMIEntry);
        btnAddBMIEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddBMIFabClick();
            }
        });

        btnUpdateProfile = (FloatingActionButton) findViewById(R.id.btnUpdateProfile);
        btnUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onProfileFabClick();
            }
        });


    }

    public void onAddBMIFabClick(){
        fabClicked = FabClickedEnum.AddBMIEntry;
        //sheet.expandFab();
        onFabAnimationEnd();
    }

    public void onProfileFabClick(){
        fabClicked = FabClickedEnum.Profile;
        //sheet.expandFab();
        onFabAnimationEnd();
    }

    public void onFabAnimationEnd(){


        switch (fabClicked) {
            case AddBMIEntry:
                navigateToBMIAddActivity();
                break;
            case Profile:
                navigateToProfileActivity();
                break;
        }
    }

    public void navigateToBMIAddActivity(){
        Intent newIntent = new Intent(this,BMIEntryActivity.class);
        startActivity(newIntent);
    }

    public void navigateToProfileActivity(){

        Intent newIntent = new Intent(this,ProfileActivity.class);
        startActivity(newIntent);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        if(uri != null){
            String url = uri.toString();
            if(url.indexOf("/list") > -1)
                Toast.makeText(BMIListActivity.this, "Listings refreshed", Toast.LENGTH_SHORT).show();

            else if(url.indexOf("/graph") > -1)
                navToGraphView();
        }
    }
}
