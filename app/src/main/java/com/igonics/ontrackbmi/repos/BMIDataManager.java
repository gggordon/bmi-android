package com.igonics.ontrackbmi.repos;

import android.app.Activity;

import com.igonics.ontrackbmi.helpers.BMIDBHelper;
import com.igonics.ontrackbmi.helpers.PersonDBHelper;
import com.igonics.ontrackbmi.models.BMIEntry;
import com.igonics.ontrackbmi.models.Person;

import java.util.List;

/**
 * Created by ggordon on 2016-03-31.
 */
public class BMIDataManager {
    private static BMIDBHelper _bmiRepo;
    private static PersonDBHelper _personRepo;

    static  {

    }

    private static BMIDBHelper getBMIRepo(Activity activity){
        if(_bmiRepo == null)
            _bmiRepo = new BMIDBHelper(activity);
        return _bmiRepo;
    }

    private static PersonDBHelper getPersonRepo(Activity activity){
        if(_personRepo == null)
            _personRepo = new PersonDBHelper(activity,true);
        return _personRepo;
    }

    public static Person getPerson(Activity activity){
        Person p = getPersonRepo(activity).getFirstPerson();
        p.addRecords(getAllEntries(activity));
        return p;
    }

    public static boolean updatePerson(Activity activity, Person person){
        return getPersonRepo(activity).updatePerson(person);
    }

    public static List<BMIEntry> getAllEntries(Activity activity){
        return getBMIRepo(activity).getAllEntries();
    }

    public static boolean addBMIEntry(Activity activity, BMIEntry entry){
        if(entry.personId == 0)
            entry.personId = getPerson(activity).id;
        return getBMIRepo(activity).addEntry(entry);
    }


}
