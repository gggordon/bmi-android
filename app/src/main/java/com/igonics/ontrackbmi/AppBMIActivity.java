package com.igonics.ontrackbmi;

import android.app.ActionBar;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;

import com.igonics.ontrackbmi.fragments.ToolBarMain;
//import android.widget.Toolbar;


/**
 * Created by ggordon on 2016-03-31.
 */
public abstract class AppBMIActivity extends AppCompatActivity
   implements ToolBarMain.OnFragmentInteractionListener

{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        if(uri != null){
            if(uri.toString().matches("/list"))
                navToListView();
            else if(uri.toString().matches("/graph"))
                navToGraphView();
        }
    }

    public void navToListView(){
        Intent newIntent = new Intent(this, BMIListActivity.class);
        startActivity(newIntent);
    }

    public void navToGraphView(){
        Intent newIntent = new Intent(this, MainActivity.class);
        startActivity(newIntent);

    }
}