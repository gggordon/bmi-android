package com.igonics.ontrackbmi.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.igonics.ontrackbmi.R;
import com.igonics.ontrackbmi.listeners.PromptListener;

import java.io.Serializable;


/**
 * Created by ggordon on 2016-03-31.
 */
public class TextPromptDialog extends DialogFragment {

    public static final String ARG_ON_SAVE_LISTENER = "onSaveListener";
    public static final String ARG_ON_CANCEL_LISTENER = "onCancelListener";
    public static final String ARG_TITLE = "title";
    public static final String ARG_PROMPT = "prompt";
    public static final String ARG_ICON = "icon";
    public static final String ARG_HINT = "hint";

    private EditText txtResponse;

    PromptListener mListener;

    public TextPromptDialog(){

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstance){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.fragment_text_prompt, null);
        txtResponse = (EditText) view.findViewById(R.id.txtTFPValue);

        savedInstance = savedInstance ==null ? getArguments() : savedInstance;
        savedInstance = savedInstance == null ? new Bundle() : savedInstance;

        if(savedInstance.getString(ARG_TITLE) != null)
            builder.setTitle(savedInstance.getString(ARG_TITLE));

        if(savedInstance.getString(ARG_ICON) != null)
            builder.setIcon(savedInstance.getInt(ARG_ICON));

        final DialogInterface.OnClickListener onSaveListener = savedInstance.get(ARG_ON_SAVE_LISTENER) == null ?
                null : (DialogInterface.OnClickListener) savedInstance.get(ARG_ON_SAVE_LISTENER);

        final DialogInterface.OnClickListener onCancelListener = savedInstance.get(ARG_ON_CANCEL_LISTENER) == null ?
                null : (DialogInterface.OnClickListener) savedInstance.get(ARG_ON_CANCEL_LISTENER);

        builder
                .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                       if(onSaveListener != null)
                           onSaveListener.onClick(dialog,id);
                        OnDialogClick(true);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(onCancelListener != null)
                            onCancelListener.onClick(dialog,id);
                        OnDialogClick(false);
                    }
                });

        return builder.setView(view).create();
    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);

        try{
            mListener = (PromptListener) activity;
        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString() + "must implement "+PromptListener.class);
        }
    }

    private void OnDialogClick(boolean positive){
        if(positive)
            mListener.onDialogPositiveClick(this);
        else
            mListener.onDialogNegativeClick(this);
    }

    /**
     * getResponse
     * Get Response After Event Fired
     * */
    public String getResponse(){
        if(txtResponse == null)
            return  "";
        return txtResponse.getText().toString();
    }

    public interface TextPromptDialogListener extends Serializable, PromptListener{

    }
}
