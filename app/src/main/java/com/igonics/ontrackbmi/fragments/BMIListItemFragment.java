package com.igonics.ontrackbmi.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.igonics.ontrackbmi.R;
import com.igonics.ontrackbmi.models.BMIEntry;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BMIListItemFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BMIListItemFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BMIListItemFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    public static final String ARG_BMIEntry = "bmi_entry";


    // TODO: Rename and change types of parameters
    private BMIEntry mParam1;


    private OnFragmentInteractionListener mListener;

    public BMIListItemFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param entry Parameter 1.

     * @return A new instance of fragment BMIListItem.
     */
    // TODO: Rename and change types and number of parameters
    public static BMIListItemFragment newInstance(BMIEntry entry) {
        BMIListItemFragment fragment = new BMIListItemFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_BMIEntry, entry);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = (BMIEntry)getArguments().getSerializable(ARG_BMIEntry);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bmilist_item, container, false);
        BMIEntry entry = null;
        if(savedInstanceState != null)
            entry = (BMIEntry)savedInstanceState.getSerializable(ARG_BMIEntry);
        if(entry == null)
            entry = mParam1;
        if(entry == null)
            entry = new BMIEntry();
        ImageView image = (ImageView)view.findViewById(R.id.vwBMIItemImage);
        TextView header = (TextView)view.findViewById(R.id.lblBMIItemHeader);
        TextView details = (TextView)view.findViewById(R.id.lblBMIitemDetails);
        if(image != null){
            switch (entry.getStatus().toLowerCase().replaceAll(" ","")){
                case "underweight":image.setImageResource(R.mipmap.ic_bmi_underweight);
                    break;
                case "normal":image.setImageResource(R.mipmap.ic_bmi_normal);
                    break;
                case "overweight":image.setImageResource(R.mipmap.ic_bmi_overweight);
                    break;
                case "grosslyoverweight":image.setImageResource(R.mipmap.ic_bmi_gross_overweight);
                    break;
            }
            image.invalidate();
        }
        if(header != null)
            header.setText(entry.getStatus() +" /  "+entry.getDateRecorded());
        if(details != null)
            details.setText(
                    String.format("BMI : %.2f | Weight : %.2f | Height : %.2f",
                            entry.getBMI(), entry.weight, entry.height)
            );

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
