package com.igonics.ontrackbmi.listeners;

import android.app.DialogFragment;

/**
 * Created by ggordon on 2016-03-31.
 */
public interface PromptListener {
    public void onDialogPositiveClick(DialogFragment dialog);
    public void onDialogNegativeClick(DialogFragment dialog);
}
