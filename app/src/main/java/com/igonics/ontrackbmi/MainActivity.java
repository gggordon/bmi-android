package com.igonics.ontrackbmi;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.github.fabtransitionactivity.SheetLayout;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.Entry;


import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.igonics.ontrackbmi.fragments.TextPromptDialog;
import com.igonics.ontrackbmi.fragments.TextPromptFragment;
import com.igonics.ontrackbmi.helpers.DialogHelper;
import com.igonics.ontrackbmi.listeners.PromptListener;
import com.igonics.ontrackbmi.models.BMIEntry;
import com.igonics.ontrackbmi.repos.BMIDataManager;
import com.shamanland.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class MainActivity extends AppBMIActivity
        implements SheetLayout.OnFabAnimationEndListener
{


    private enum FabClickedEnum {
        AddBMIEntry,
        Profile
    }

    private FloatingActionButton btnAddBMIEntry;
    private FloatingActionButton btnUpdateProfile;
    private LineChart chart;
    private SheetLayout sheet;
    private FabClickedEnum fabClicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showChart();
        configureEvents();

    }

    public void configureEvents(){
        btnAddBMIEntry = (FloatingActionButton) findViewById(R.id.btnAddBMIEntry);
        btnAddBMIEntry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddBMIFabClick();
            }
        });

        btnUpdateProfile = (FloatingActionButton) findViewById(R.id.btnUpdateProfile);
        btnUpdateProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onProfileFabClick();
            }
        });
        sheet = (SheetLayout) findViewById(R.id.chartViewSheet);
        sheet.setFab(btnAddBMIEntry);

        sheet.setFabAnimationEndListener(this);

    }

    public void onAddBMIFabClick(){
        fabClicked = FabClickedEnum.AddBMIEntry;
        //sheet.expandFab();
        onFabAnimationEnd();
    }

    public void onProfileFabClick(){
        fabClicked = FabClickedEnum.Profile;
        //sheet.expandFab();
        onFabAnimationEnd();
    }

    public void onFabAnimationEnd(){

        sheet.contractFab();
        switch (fabClicked) {
            case AddBMIEntry:
                navigateToBMIAddActivity();
                break;
            case Profile:
                navigateToProfileActivity();
                break;
        }
    }

    public void navigateToBMIAddActivity(){
        Intent newIntent = new Intent(this,BMIEntryActivity.class);
        startActivity(newIntent);
    }

    public void navigateToProfileActivity(){

        Intent newIntent = new Intent(this,ProfileActivity.class);
        startActivity(newIntent);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        if(uri != null){
            String url = uri.toString();
            if(url.indexOf("/list") > -1)
                navToListView();
            else if(url.indexOf("/graph") > -1)
                Toast.makeText(MainActivity.this, "Your graph has been updated", Toast.LENGTH_SHORT).show();
        }
    }



    public void showChart(){
        try {
            /*
            List<BMIEntry> entries = new ArrayList<BMIEntry>();
            for (float i = 110; i < 130; i++) {
                entries.add(new BMIEntry(i, new Float((i / 2) + (Math.random() * 12))));
            }
            */
            List<BMIEntry> entries = BMIDataManager.getAllEntries(this);
            chart = (LineChart) findViewById(R.id.vwChartHistory);
            chart.setNoDataText("Add a new BMI Entry to see your chart");
            if(entries.size() == 0) {
                chart.invalidate();
                return;
            }
            ArrayList<Entry> bmiEntries = new ArrayList<Entry>();
            ArrayList<String> dataSetLabels = new ArrayList<String>();
            Collections.sort(entries, new Comparator<BMIEntry>() {
                @Override
                public int compare(BMIEntry lhs, BMIEntry rhs) {
                    return -1;
                }
            });

            for (int i = 0; i < entries.size(); i++) {
                bmiEntries.add(new Entry(entries.get(i).getBMI(), i));
                dataSetLabels.add(entries.get(i).getDateRecorded());
            }

            LineDataSet bmiEntryDataSet = new LineDataSet(bmiEntries, "BMI");
            bmiEntryDataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
            bmiEntryDataSet.disableDashedHighlightLine();
            bmiEntryDataSet.disableDashedLine();

            ArrayList<ILineDataSet> chartDataSets = new ArrayList<ILineDataSet>();
            chartDataSets.add(bmiEntryDataSet);



            LineData chartData = new LineData(dataSetLabels, chartDataSets);


            chart.setData(chartData);

            chart.notifyDataSetChanged();
            chart.invalidate();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
