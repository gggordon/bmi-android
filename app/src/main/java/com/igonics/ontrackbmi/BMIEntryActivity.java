package com.igonics.ontrackbmi;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Date;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.igonics.ontrackbmi.models.BMIEntry;
import com.igonics.ontrackbmi.models.Person;
import com.igonics.ontrackbmi.repos.BMIDataManager;
import com.shamanland.fab.FloatingActionButton;

public class BMIEntryActivity extends AppBMIActivity{

    private FloatingActionButton btnCancel;
    private FloatingActionButton btnSave;
    private FloatingActionButton btnChooseDate;
    private EditText txtWeight;
    private EditText txtHeight;
    private TextView lblDateRecorded;
    private Date dateRecorded;
    private Person person;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bmientry);

        lblDateRecorded = (TextView) findViewById(R.id.lblDateRecordedValue);
        txtWeight = (EditText) findViewById(R.id.txtWeight);
        txtHeight = (EditText) findViewById(R.id.txtHeight);

        person = BMIDataManager.getPerson(this);

        configureEvents();
    }

    public void configureEvents(){
        btnCancel = (FloatingActionButton) findViewById(R.id.btnSaveUpdatedProfile);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancelFabClick(true);
            }
        });

        btnSave = (FloatingActionButton) findViewById(R.id.btnSaveBMIEntry);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveFabClick();
            }
        });

        btnChooseDate = (FloatingActionButton) findViewById(R.id.btnChooseDate);

        btnChooseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(new SlideDateTimeListener() {

                            @Override
                            public void onDateTimeSet(Date date) {
                                setTxtDate(date);
                            }

                            @Override
                            public void onDateTimeCancel() {
                                // Overriding onDateTimeCancel() is optional.
                            }
                        })
                        .setInitialDate(new Date())
                        .build()
                        .show();
            }
        });



    }

    private void setTxtDate(Date date){
        lblDateRecorded.setText(date.toString());
        dateRecorded = date;
    }

    private void onCancelFabClick(boolean navToMainActivity){
        lblDateRecorded.setText("");
        txtHeight.setText("0");
        txtWeight.setText("0");
        if(navToMainActivity) {
            Intent newIntent = new Intent(this, MainActivity.class);
            startActivity(newIntent);
        }

    }

    private void onSaveFabClick(){
        //save bmi entry
        Float weight = Float.parseFloat(txtWeight.getText().toString()),
              height = Float.parseFloat(txtHeight.getText().toString());
        if(BMIDataManager.addBMIEntry(this, new BMIEntry(0,person.id,weight,height,dateRecorded))){
            Toast.makeText(BMIEntryActivity.this, "Entry Saved Successfully", Toast.LENGTH_SHORT).show();
            onCancelFabClick(false);
        }else{
            Toast.makeText(BMIEntryActivity.this, "We were unable to save this entry, please try again", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        if(uri != null){
            String url = uri.toString();
            if(url.indexOf("/list") > -1)
                navToListView();
            else if(url.indexOf("/graph") > -1)
                navToGraphView();
        }
    }


}
