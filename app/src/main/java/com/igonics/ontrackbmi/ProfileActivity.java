package com.igonics.ontrackbmi;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener;
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker;
import com.igonics.ontrackbmi.helpers.OwnerHelper;
import com.igonics.ontrackbmi.helpers.PersonDBHelper;
import com.igonics.ontrackbmi.models.BMIEntry;
import com.igonics.ontrackbmi.models.Person;
import com.igonics.ontrackbmi.repos.BMIDataManager;
import com.shamanland.fab.FloatingActionButton;

import java.util.Date;

public class ProfileActivity extends AppBMIActivity {

    private FloatingActionButton btnCancel;
    private FloatingActionButton btnSave;
    private FloatingActionButton btnChooseDateOfBirth;
    private EditText txtUsername;
    private EditText txtPassword;
    private EditText txtHealthCareNo;
    private TextView lblDateOfBirth;
    private Date dateRecorded;
    private static PersonDBHelper personDBHelper;
    private Person person;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        person = BMIDataManager.getPerson(this);
        dateRecorded = person.dateOfBirth;
        configureView();

    }

    private void configureView(){
        try {

            ImageView vwProfileImage = (ImageView) findViewById(R.id.vwProfileImage);
            vwProfileImage.setImageURI(OwnerHelper.getContactPhoto(this));
            if(!OwnerHelper.isIsValidPhotoId() )
                vwProfileImage.setImageResource(R.mipmap.ic_launcher);
            vwProfileImage.invalidate();
        }catch (Exception e){
            Log.e("Photo Retrieval","Unable to retrieve photo",e);
        }

        //------------

        lblDateOfBirth = (TextView) findViewById(R.id.lblDateOfBirthValue);
        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtHealthCareNo = (EditText) findViewById(R.id.txtHealthCareNo);
        refreshViewWithPersonDetails();

        configureEvents();

    }

    private void configureEvents(){
        btnCancel = (FloatingActionButton) findViewById(R.id.btnCancelUpdatedProfile);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCancelFabClick(true);
            }
        });

        btnSave = (FloatingActionButton) findViewById(R.id.btnSaveUpdatedProfile);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSaveFabClick();
            }
        });

        btnChooseDateOfBirth = (FloatingActionButton) findViewById(R.id.btnChooseDateOfBirth);

        btnChooseDateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SlideDateTimePicker.Builder(getSupportFragmentManager())
                        .setListener(new SlideDateTimeListener() {

                            @Override
                            public void onDateTimeSet(Date date) {
                                setTxtDate(date);
                            }

                            @Override
                            public void onDateTimeCancel() {
                                // Overriding onDateTimeCancel() is optional.
                            }
                        })
                        .setInitialDate(new Date())
                        .build()
                        .show();
            }
        });


    }

    private void setTxtDate(Date date){
        lblDateOfBirth.setText(date.toString());
        String m = date.toString();
        dateRecorded = date;

    }

    private void onCancelFabClick(boolean navToMain){
        //refresh details from repo, populate form, return to main activity
        refreshViewWithPersonDetails();
        if(navToMain) {
            Intent newIntent = new Intent(this, MainActivity.class);
            startActivity(newIntent);
        }

    }

    private void refreshViewWithPersonDetails(){
        lblDateOfBirth.setText(person.dateOfBirth.toString());
        txtUsername.setText(person.username);
        txtPassword.setText("");
        txtHealthCareNo.setText(person.healthCareNo);
        BMIEntry mostRecentEntry = person.getMostRecentBMI();

        TextView lblBMIStatus = (TextView )findViewById(R.id.info_value_status);
        TextView lblLastRecordedDate = (TextView )findViewById(R.id.info_value_recordate);
        TextView lblBMIValue = (TextView )findViewById(R.id.info_value_current_bmi);

        if(mostRecentEntry != null){
            lblBMIStatus.setText(mostRecentEntry.getStatus());
            lblLastRecordedDate.setText(mostRecentEntry.getDateRecorded());
            lblBMIValue.setText(String.format("%.2f",mostRecentEntry.getBMI()));

        }else{
            lblBMIStatus.setText("N/A");
            lblLastRecordedDate.setText("N/A");
            lblBMIValue.setText("N/A");

        }

    }

    private void onSaveFabClick(){
        if(person == null){
            person = new Person();
        }
        person.dateOfBirth = dateRecorded;
        person.agreeToDisclaimer = false;
        person.username = txtUsername.getText().toString();
        if(txtPassword.getText().toString().length() != 0)
            person.password = txtPassword.getText().toString();
        person.healthCareNo = txtHealthCareNo.getText().toString();
        
        if( BMIDataManager.updatePerson(this,person) ){
            Toast.makeText(ProfileActivity.this, "Details Updated Successfully", Toast.LENGTH_SHORT).show();
            onCancelFabClick(false);
        }else{
            Toast.makeText(ProfileActivity.this, "We were unable to update your details, please try again", Toast.LENGTH_SHORT).show();
        }

        
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        if(uri != null){
            String url = uri.toString();
            if(url.indexOf("/list") > -1)
                navToListView();
            else if(url.indexOf("/graph") > -1)
                navToGraphView();
        }
    }

}
